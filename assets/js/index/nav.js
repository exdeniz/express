$(".js-scroll-to").click(function (e) {
    var target = $($(this).attr("href"));

    if (target.offset()) {
        // $('html, body').animate({
        //     scrollTop: target.offset().top + 'px'
        // }, 600);
        $("html, body").velocity("scroll", {
            offset: target.offset().top + "px",
            duration: 750,
            easing: "easeInOutCirc"
        });
    }

    e.preventDefault();
});

$(".js-next").click(function (e) {

    var selected = $(".js-list-item.js-current-panel");
    var anchors = $(".js-list-item");

    var pos = anchors.index(selected);
    var next = anchors.get(pos + 1);
    // var prev = anchors.get(pos - 1);

    var target = $(next);

    $(selected).removeClass("js-current-panel");
    $(next).addClass("js-current-panel");

    if (target.offset()) {
        // $('html, body').animate({
        //     scrollTop: target.offset().top + 'px'
        // }, 600);
        $("html, body").velocity("scroll", {
            offset: target.offset().top + "px",
            duration: 750,
            easing: "easeInOutCirc"
        });
    }

    e.preventDefault();
});

$(".js-prev").click(function (e) {

    var selected = $(".js-list-item.js-current-panel");
    var anchors = $(".js-list-item");

    var pos = anchors.index(selected);
    // var next = anchors.get(pos + 1);
    var prev = anchors.get(pos - 1);

    var target = $(prev);

    $(selected).removeClass("js-current-panel");
    $(prev).addClass("js-current-panel");

    if (target.offset()) {
        // $('html, body').animate({
        //     scrollTop: target.offset().top + 'px'
        // }, 600);
        $("html, body").velocity("scroll", {
            offset: target.offset().top + "px",
            duration: 750,
            easing: "easeInBack"
        });
    }

    e.preventDefault();
});

$(".navShareBlock").click(function () {
    if ($(this).hasClass("open")) {
        $(this).removeClass("open")
        iconsHide();

    } else {
        $(this).addClass("open");
        iconsShow();
    }
});

function iconsShow(argument) {
    var startTranslate = -40;
    $(".navShareBlock").find(".navShareIcon").each(function () {
        $(this).velocity(
                     {translateX: startTranslate, opacity: 1}, "easeOutBounce", 500
                 );
        startTranslate = startTranslate - 40
    });
}

function iconsHide(argument) {
    $(".navShareBlock").find(".navShareIcon").each(function () {
        $(this).velocity(
                     {translateX: -1, opacity: 1}, "easeOutBounce", 500
                 );
        $(this).velocity(
                     {opacity: 0}, "easeOutBounce", 10
                 );
    });
}
