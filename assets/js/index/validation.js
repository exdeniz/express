$(".bookingForm").validate({
    submitHandler: function(form) {
        form.submit();
    },
    highlight: function(element) {
        $(element).addClass("inputError");
    },
    unhighlight: function(element) {
        $(element).removeClass("inputError");
    },
    focusInvalid: false,
    invalidHandler: function(form, validator) {

        if (!validator.numberOfInvalids())
            return;

        $('html, body').animate({
            scrollTop: $(validator.errorList[0].element).offset().top
        }, 500);

    }
});


$(".popForm").validate({
    submitHandler: function(form) {
        form.submit();
    },
    highlight: function(element) {
        $(element).addClass("inputError");
    },
    unhighlight: function(element) {
        $(element).removeClass("inputError");
    },
    focusInvalid: false,
    invalidHandler: function(form, validator) {

        if (!validator.numberOfInvalids())
            return;

        $('html, body').animate({
            scrollTop: $(validator.errorList[0].element).offset().top
        }, 500);

    }
});
