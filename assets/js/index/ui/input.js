;(function($) {
    function Placeholder(elem, options) {
        var self = this;
        self.$elem = $(elem);

        var defaults = {
            require: true
        };

        self.options = $.extend({}, defaults, options);
        init();


        function init() {
            self.$elem.focusin(hideLabel);
            self.$elem.focusout(showLabel);
        }


        function hideLabel(event) {
            checkError();
            self.$elem.children('label').hide();
        }

        function showLabel(event) {
            if(!self.$elem.children('input, textarea').val()) {
                self.$elem.children('label').show();
            }
        }
        function checkError() {
            if (self.$elem.hasClass('inputLabelError')) {
                self.$elem.removeClass('inputLabelError')
            }

        }

    }


    $.fn.placeholder = function(options) {
        return this.each(function() {
            // prevent multiple instantiation
            if (!$(this).data('placeholder'))
                $(this).data('placeholder', new Placeholder(this, options));
        });
    };
})(jQuery);

$('.inputLabel').placeholder();
