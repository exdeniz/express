$('.selectric').selectric({
    disableOnMobile: false
})
$('.datepicker').pickadate()
$('.timepicker').pickatime()
$('.icheck').iCheck()
$('.banner').slick({
    dots: true,
    arrows: false
})
$('.photo').slick({
    dots: false,
    arrows: true
})

$('#mobileMenu').dcAccordion({
    eventType: 'click',
    autoClose: true,
    saveState: false,
    disableLink: true,
    showCount: true,
    speed: 'fast',
    classExpand: 'mobileMenuItem',
    showCount: false
})
