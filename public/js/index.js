
$(".js-scroll-to").click(function (e) {
    var target = $($(this).attr("href"));

    if (target.offset()) {
        // $('html, body').animate({
        //     scrollTop: target.offset().top + 'px'
        // }, 600);
        $("html, body").velocity("scroll", {
            offset: target.offset().top + "px",
            duration: 750,
            easing: "easeInOutCirc"
        });
    }

    e.preventDefault();
});

$(".js-next").click(function (e) {

    var selected = $(".js-list-item.js-current-panel");
    var anchors = $(".js-list-item");

    var pos = anchors.index(selected);
    var next = anchors.get(pos + 1);
    // var prev = anchors.get(pos - 1);

    var target = $(next);

    $(selected).removeClass("js-current-panel");
    $(next).addClass("js-current-panel");

    if (target.offset()) {
        // $('html, body').animate({
        //     scrollTop: target.offset().top + 'px'
        // }, 600);
        $("html, body").velocity("scroll", {
            offset: target.offset().top + "px",
            duration: 750,
            easing: "easeInOutCirc"
        });
    }

    e.preventDefault();
});

$(".js-prev").click(function (e) {

    var selected = $(".js-list-item.js-current-panel");
    var anchors = $(".js-list-item");

    var pos = anchors.index(selected);
    // var next = anchors.get(pos + 1);
    var prev = anchors.get(pos - 1);

    var target = $(prev);

    $(selected).removeClass("js-current-panel");
    $(prev).addClass("js-current-panel");

    if (target.offset()) {
        // $('html, body').animate({
        //     scrollTop: target.offset().top + 'px'
        // }, 600);
        $("html, body").velocity("scroll", {
            offset: target.offset().top + "px",
            duration: 750,
            easing: "easeInBack"
        });
    }

    e.preventDefault();
});

$(".navShareBlock").click(function () {
    if ($(this).hasClass("open")) {
        $(this).removeClass("open")
        iconsHide();

    } else {
        $(this).addClass("open");
        iconsShow();
    }
});

function iconsShow(argument) {
    var startTranslate = -40;
    $(".navShareBlock").find(".navShareIcon").each(function () {
        $(this).velocity(
                     {translateX: startTranslate, opacity: 1}, "easeOutBounce", 500
                 );
        startTranslate = startTranslate - 40
    });
}

function iconsHide(argument) {
    $(".navShareBlock").find(".navShareIcon").each(function () {
        $(this).velocity(
                     {translateX: -1, opacity: 1}, "easeOutBounce", 500
                 );
        $(this).velocity(
                     {opacity: 0}, "easeOutBounce", 10
                 );
    });
}

$('.partnerSlider').slick({
  infinite: false,
  slidesToShow: 4,
  slidesToScroll: 4
});

$('.js-changephoneMsk').click(function () {
    $('.headerAddress a').removeClass('disable')
    $(this).addClass('disable')
    $('.headerPhoneRussia').hide()
    $('.headerPhoneMsk').show()
})
$('.js-changephoneRus').click(function () {
    $('.headerAddress a').removeClass('disable')
    $(this).addClass('disable')
    $('.headerPhoneRussia').show()
    $('.headerPhoneMsk').hide()
})

$('.js-popupCall').click(function () {
    $('.overley').show()
    $('.popup').show()

});



$('.js-popupclose').click(function () {
    $('.overley').hide()
    $('.popup').hide()
});

$('.js-changeroom').on('selectric-change', function (element) {
    console.log($(this).val())
    $('.bookingFormItemRoom').hide();
    $('.'+$(this).val()).show();
})



$(document).ready(function () {
  $('.bookingFormItemRoom').hide();
  $('.room1').show();
})

$('.practiceItemSliderSlides').slick({
    infinite: true,
    dots: true
});

$(".bookingForm").validate({
    submitHandler: function(form) {
        form.submit();
    },
    highlight: function(element) {
        $(element).addClass("inputError");
    },
    unhighlight: function(element) {
        $(element).removeClass("inputError");
    },
    focusInvalid: false,
    invalidHandler: function(form, validator) {

        if (!validator.numberOfInvalids())
            return;

        $('html, body').animate({
            scrollTop: $(validator.errorList[0].element).offset().top
        }, 500);

    }
});


$(".popForm").validate({
    submitHandler: function(form) {
        form.submit();
    },
    highlight: function(element) {
        $(element).addClass("inputError");
    },
    unhighlight: function(element) {
        $(element).removeClass("inputError");
    },
    focusInvalid: false,
    invalidHandler: function(form, validator) {

        if (!validator.numberOfInvalids())
            return;

        $('html, body').animate({
            scrollTop: $(validator.errorList[0].element).offset().top
        }, 500);

    }
});

;(function($) {
    function Placeholder(elem, options) {
        var self = this;
        self.$elem = $(elem);

        var defaults = {
            require: true
        };

        self.options = $.extend({}, defaults, options);
        init();


        function init() {
            self.$elem.focusin(hideLabel);
            self.$elem.focusout(showLabel);
        }


        function hideLabel(event) {
            checkError();
            self.$elem.children('label').hide();
        }

        function showLabel(event) {
            if(!self.$elem.children('input, textarea').val()) {
                self.$elem.children('label').show();
            }
        }
        function checkError() {
            if (self.$elem.hasClass('inputLabelError')) {
                self.$elem.removeClass('inputLabelError')
            }

        }

    }


    $.fn.placeholder = function(options) {
        return this.each(function() {
            // prevent multiple instantiation
            if (!$(this).data('placeholder'))
                $(this).data('placeholder', new Placeholder(this, options));
        });
    };
})(jQuery);

$('.inputLabel').placeholder();

$('.selectric').selectric({
    disableOnMobile: false
})
$('.datepicker').pickadate()
$('.timepicker').pickatime()
$('.icheck').iCheck()
$('.banner').slick({
    dots: true,
    arrows: false
})
$('.photo').slick({
    dots: false,
    arrows: true
})

$('#mobileMenu').dcAccordion({
    eventType: 'click',
    autoClose: true,
    saveState: false,
    disableLink: true,
    showCount: true,
    speed: 'fast',
    classExpand: 'mobileMenuItem',
    showCount: false
})
