var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    prefix = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    postcss = require('gulp-postcss'),
    assets = require('postcss-assets'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    jeet = require('jeet'),
    rupture = require('rupture'),
    normalize = require('normalize-stylus'),
    sourcemaps = require('gulp-sourcemaps'),
    reload = browserSync.reload;

// Собираем Stylus
gulp.task('stylus', function () {
    var processors = [
                assets({
                    loadPaths: ['assets/icons/']
                })
            ];
    gulp.src('./assets/b/**/*.styl')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(stylus({
                use: [ jeet(), rupture(), normalize() ],
                import: [ 'jeet', 'rupture', 'normalize' ]
            }))
        .pipe(concat('blocks.css'))
        .pipe(postcss(processors))
        .pipe(prefix({
                browsers: [ 'last 2 versions', 'IE 9', 'IE 10' ]
            }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public/css/')) // записываем css
        .pipe(reload({ stream: true }));
});
