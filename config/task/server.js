var gulp = require('gulp'),
    express = require("express"),
    fs = require('fs'),
    favicon = require('serve-favicon'),
    compression = require('compression'),
    browserSync = require('browser-sync'),
    Jade = require('jade'),
    hapi = require('hapi'),
    path = require('path'),

    reload = browserSync.reload;


gulp.task('express', function() {
    var app = express();
    app.set('views', './assets/template');
    app.use(favicon('./public/favicon.ico'));
    app.set('view engine', 'jade');

    app.use(compression({
        threshold: 512
    }));
    app.use(express.static('./public'));
    app.listen(9001);
    app.get('/', function(req, res) {
        res.render('index');
    });
    app.get('/:file', function(req, res) {
        res.render(req.params.file);
    });
    console.log('Listening on port: 9001');
});
gulp.task('hapi', function() {
    var server = new hapi.Server();

    // add connection parameters
    server.connection({
        host: 'localhost',
        port: 9001
    });

    server.views({
        engines: {
            jade: {
                module: require('jade'),
                isCached: false
            }
        },
        path: 'assets/template'
            // layoutPath: 'assets/template',
            // layout: 'default',
            // //helpersPath: 'views/helpers',
            //partialsPath: 'views/partials'
    });

    // create your routes, currently it's just one
    var routes = [{
        method: 'GET',
        path: '/favicon.ico',
        handler: {
            file: './favicon.ico'
        }
        
    }, {
        method: 'GET',
        path: '/',
        handler: function(request, reply) {
            // Render the view with the custom greeting
            var data = {
                title: 'This is Index!',
                message: 'Hello, World. You crazy handlebars layout'
            };

            return reply.view('index', data);
        }
    }, {
        method: 'GET',
        path: '/{filename}',
        handler: function(request, reply) {
            // Render the view with the custom greeting
            var data = {
                title: 'This is Index!',
                message: 'Hello, World. You crazy handlebars layout'
            };

            return reply.view(request.params.filename, data);
        }
    }, {
        path: "/{path*}",
        method: "GET",
        handler: {
            directory: {
                path: "./public",
                listing: false,
                index: false
            }
        }
    }];

    // tell your server about the defined routes
    server.route(routes);

    // Start the server
    server.register([

        {
            register: require("hapi-cache-buster")
        }
    ], function() {
        //Start the server
        server.start(function() {
            //Log to the console the host and port info
            console.log('Server started at: ' + server.info.uri);
        });


    })
});


gulp.task('express-pagespeed', function() {
    var app = express();
    // app.set('views', __dirname + './assets/template');
    // app.set('view engine', 'jade');
    app.use(compression({
        threshold: 512
    }));
    app.use(express.static(__dirname + './build'));
    app.listen(9001);
    app.get('/', function(req, res) {
        res.render('index');
    });
    app.get('/:file', function(req, res) {
        res.render(req.params.file);
    });
    console.log('Listening on port: 9001');
});

gulp.task('browser-sync', function() {
    browserSync({
        proxy: "localhost:9001",
        //tunnel: true,
        browser: "google chrome",
        startPath: "/index"
    });
});
