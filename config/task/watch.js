// Запуск сервера разработки gulp watch
var gulp = require('gulp'),
    batch = require('gulp-batch')
    watch = require('gulp-watch');

gulp.task('default', function() {
    // // Предварительная сборка проекта
    gulp.run('stylus');
    gulp.run('jadeMixin');
    gulp.run('jade');
    gulp.run('images');
    gulp.run('favicon');
    gulp.run('vendor-js');
    gulp.run('js');
    gulp.run('svgicon');
    gulp.run('fonts');
    gulp.run('hapi');
    gulp.run('browser-sync');


    // gulp.watch('assets/b/**/*.styl', function() {
    //     gulp.run('stylus2');
    // });
    watch('assets/b/**/*.styl', function () {

            gulp.start('stylus');
    });

    // watch(['assets/template/**/*.jade', '/assets/b/**/*.jade'], function() {
    //     gulp.start(['jadeMixin', 'jade']);
    // });
    //

    watch(['assets/template/**/*.jade', '/assets/b/**/*.jade'], batch(function (events, done) {
        gulp.start('jade', done);
    }));

    gulp.watch('/assets/b/**/*.jade', function() {
        gulp.run('jadeMixin');
    });

    gulp.watch('assets/img/**/*', function() {
        gulp.run('images');
    });
    gulp.watch('assets/favicon/**/*', function() {
        gulp.run('favicon');
    });
    gulp.watch('assets/js/**/*', function() {
        gulp.run('js');
        gulp.run('vendor-js');
    });
    gulp.watch('assets/fonts/**/*', function() {
        gulp.run('fonts');
    });
    // gulp.watch('assets/icons/**/*', function() {
    //     gulp.run('icons');
    // });
    gulp.watch('assets/svg/**/*', function() {
        gulp.run('svgicon');
    });
});
